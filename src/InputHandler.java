import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler(Model model){
        this.model=model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.NUMPAD0) {
            model.spielerWechseln();
            model.steinSetzen(0);

        } else if (key == KeyCode.NUMPAD1) {
            model.spielerWechseln();
            model.steinSetzen(1);

        } else if (key == KeyCode.NUMPAD2) {
            model.spielerWechseln();
            model.steinSetzen(2);

        } else if (key == KeyCode.NUMPAD3) {
            model.spielerWechseln();
            model.steinSetzen(3);

        } else if (key == KeyCode.NUMPAD4) {
            model.spielerWechseln();
            model.steinSetzen(4);

        } else if (key == KeyCode.NUMPAD5) {
            model.spielerWechseln();
            model.steinSetzen(5);

        } else if (key == KeyCode.NUMPAD6) {
            model.spielerWechseln();
            model.steinSetzen(6);

        } else if (key == KeyCode.NUMPAD7) {
            model.spielerWechseln();
            model.steinSetzen(7);

        }else if (key == KeyCode.SPACE) {
            model.reset();
        }





    }

    public void onKeyReleased(KeyCode key) {

    }
}


