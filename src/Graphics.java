import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Graphics {

    private Model model;
    private GraphicsContext gc;


    public static final int WIDTH = 800;
    public static final int HEIGHT = 800;

    public static final int STEINGROESSE = 80;
    public static final int LININEBREITE = 10;
    public static final int SPIELFELD_OFFSET_X = 80;
    public static final int SPIELFELD_OFFSET_Y = 80;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;

    }


    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public void draw() {
        gc.clearRect(0, 0, WIDTH, HEIGHT);

        byte[][] brett = model.getSpielbrett();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {

                if (brett[x][y] == 1) {
                    gc.setFill(Color.RED);
                    gc.fillOval((x * STEINGROESSE) + SPIELFELD_OFFSET_X, (y * STEINGROESSE) + SPIELFELD_OFFSET_Y, STEINGROESSE, STEINGROESSE);
                } else if (brett[x][y] == 2) {
                    gc.setFill(Color.ORANGE);
                    gc.fillOval((x * STEINGROESSE) + SPIELFELD_OFFSET_Y, (y * STEINGROESSE) + SPIELFELD_OFFSET_Y, STEINGROESSE, STEINGROESSE);
                }

            }

        }


        for (int i = 0; i < 9; i++) {


            gc.setFill(Color.DARKBLUE);
            gc.fillRect(SPIELFELD_OFFSET_X - LININEBREITE / 2, (i * STEINGROESSE) - LININEBREITE / 2 + SPIELFELD_OFFSET_Y,
                    STEINGROESSE * 8 + LININEBREITE, LININEBREITE);
            gc.fillRect((i * STEINGROESSE) - LININEBREITE / 2 + SPIELFELD_OFFSET_X, SPIELFELD_OFFSET_Y - LININEBREITE / 2,
                    LININEBREITE, STEINGROESSE * 8 + LININEBREITE);
            if (i < 8) {
                gc.setFont(new Font(40));
                gc.fillText(i + "", SPIELFELD_OFFSET_X + 30 + i * STEINGROESSE, SPIELFELD_OFFSET_Y + STEINGROESSE * 8 + 40);
            }
        }


        if (model.getGewinner() == 2) {
            gc.setFont(new Font(30));
            gc.setFill(Color.ORANGE);
            gc.fillText("Das Spiel ist beendet! Gewonnen hat Gelb!", SPIELFELD_OFFSET_X + STEINGROESSE / 2, SPIELFELD_OFFSET_Y / 3);

        } else if (model.getGewinner() == 1) {
            gc.setFont(new Font(30));
            gc.setFill(Color.RED);
            gc.fillText("Das Spiel ist beendet! Gewonnen hat Rot!", SPIELFELD_OFFSET_X + STEINGROESSE / 2, SPIELFELD_OFFSET_Y / 3);
        } else {
            gc.setFont(new Font(30));
            gc.setFill(Color.BLUE);
            gc.fillText("Vier Gewinnt!", SPIELFELD_OFFSET_X + STEINGROESSE * 3, SPIELFELD_OFFSET_Y / 3);
        }

        gc.setFont(new Font(20));
        gc.setFill(Color.RED);
        gc.fillText("Score: " + model.getGewinnerScorePlayer1(), SPIELFELD_OFFSET_X + LININEBREITE, 60);
        gc.setFill(Color.ORANGE);
        gc.fillText("Score: " + model.getGewinnerScorePlayer2(), SPIELFELD_OFFSET_X + STEINGROESSE * 7 + LININEBREITE, 60);

        if (model.getGewinnArt() == 1) {
            gc.setLineWidth(5.5);
            gc.setStroke(Color.LIME);
            gc.strokeRect(model.getGewinnSpalte() * STEINGROESSE + SPIELFELD_OFFSET_X, model.getGewinnZeile() * STEINGROESSE + SPIELFELD_OFFSET_Y, STEINGROESSE, STEINGROESSE * 4);
        } else if (model.getGewinnArt() == 2) {
            gc.setLineWidth(5.5);
            gc.setStroke(Color.LIME);
            gc.strokeRect(model.getGewinnSpalte() * STEINGROESSE + SPIELFELD_OFFSET_X, model.getGewinnZeile() * STEINGROESSE + SPIELFELD_OFFSET_Y, STEINGROESSE * 4, STEINGROESSE);

        } else if (model.getGewinnArt() == 3) {
            for (int s = model.getGewinnSpalte(); s <= model.getGewinnSpalte() + 3; s++) {
                for (int z = model.getGewinnZeile(); z <= model.getGewinnZeile() + 3; z++) {
                    if (model.getGewinnSpalte() - model.getGewinnZeile() == s - z) {

                        ausgelagerteMethode(s, z);
                    }


                }
            }
        } else if (model.getGewinnArt() == 4) {
            for (int s = model.getGewinnSpalte(); s <= model.getGewinnSpalte() + 3; s++) {
                for (int z = model.getGewinnZeile(); z >= model.getGewinnZeile() - 3; z--) {
                    if (model.getGewinnSpalte() + model.getGewinnZeile() == s + z) {

                        ausgelagerteMethode(s, z);
                    }
                }
            }
        }
    }
    private void ausgelagerteMethode(int s, int z) {
        gc.setLineWidth(5.5);
        gc.setStroke(Color.LIME);
        gc.strokeRect(s * STEINGROESSE + SPIELFELD_OFFSET_X, z * STEINGROESSE + SPIELFELD_OFFSET_Y, STEINGROESSE, STEINGROESSE);
    }

}
